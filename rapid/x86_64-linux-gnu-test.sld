;;; Rapid Scheme --- An implementation of R7RS

;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid x86_64-linux-gnu-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid object-module)
	  (rapid x86_64-linux-gnu))
  (begin
    (define (object-module->bytevector object-module)
      (let ((port (open-output-bytevector)))
	(object-module-write object-module port)
	(get-output-bytevector port)))
    
    (define (run-tests)
      (test-begin "x86_64-linux-gnu target")

      (test-assert "object-write"
	(object-module->bytevector
         (make-object-module (make-section #f)
			     (make-section #f)
			     (make-section #f)
			     '()
			     '())))

      (test-end))))
