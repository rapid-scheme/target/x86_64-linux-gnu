;;; Rapid Scheme --- An implementation of R7RS

;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define osabi/system-v #x0)
(define machine/x86-64 #x003E)

(define relocation-type/x86-64-pc8 15)
(define relocation-type/x86-64-pc16 13)
(define relocation-type/x86-64-pc32 2)
(define relocation-type/x86-64-pc64 24)
(define relocation-type/x86-64-8 14)
(define relocation-type/x86-64-16 12)
(define relocation-type/x86-64-32 10)
(define relocation-type/x86-64-64 1)
(define relocation-type/x86-64-gotpcrel 9)

(define (x86-64/relocation-type relocation external?)
  (cond
   (external?
    (cond
     ((relocation-relative? relocation)
      (case (relocation-size relocation)
	((4) relocation-type/x86-64-gotpcrel)))))
   (else
    (cond
     ((relocation-relative? relocation)
      (case (relocation-size relocation)
	((1) relocation-type/x86-64-pc8)
	((2) relocation-type/x86-64-pc16)
	((4) relocation-type/x86-64-pc32)
	((8) relocation-type/x86-64-pc64)))
     (else
      (case (relocation-size relocation)
	((1) relocation-type/x86-64-8)
	((2) relocation-type/x86-64-16)
	((4) relocation-type/x86-64-32)
	((8) relocation-type/x86-64-64)))))))

(define (offset+padding base align)
  (let ((padding (modulo (- base) align)))
    (values (+ base padding) padding)))

(define (relocations elf section object-module first-import-index)
  (let ((port (open-output-bytevector)))
    ;; Relocations relative to text
    (for-each (lambda (relocation)
		(when (eq? section (relocation-section relocation))
		  (elf-write-rela-table-entry elf
					      (relocation-offset relocation)
					      1 ;; text in symtab
					      (x86-64/relocation-type relocation #f)
					      (relocation-addend relocation)
					      port)))
	      (section-relocations (object-module-text object-module)))
    ;; Relocations relative to data
    (for-each (lambda (relocation)
		(when (eq? section (relocation-section relocation))
		  (elf-write-rela-table-entry elf
					      (relocation-offset relocation)
					      2 ;; data in symtab
					      (x86-64/relocation-type relocation #f)
					      (relocation-addend relocation)
					      port)))
	      (section-relocations (object-module-data object-module)))
    ;; Relocations relative to bss
    (for-each (lambda (relocation)
		(when (eq? section (relocation-section relocation))
		  (elf-write-rela-table-entry elf
					      (relocation-offset relocation)
					      3 ;; bss in symtab
					      (x86-64/relocation-type relocation #f)
					      (relocation-addend relocation)
					      port)))
	      (section-relocations (object-module-bss object-module)))
    ;; Relocations relative to imports
    (do ((imports (object-module-imports object-module) (cdr imports))
	 (index first-import-index (+ index 1)))
	((null? imports))
      (for-each (lambda (relocation)
		  (when (eq? section (relocation-section relocation))
		    (elf-write-rela-table-entry elf
						(relocation-offset relocation)
						index
						(x86-64/relocation-type relocation #t)
						(relocation-addend relocation)
						port)))
		(import-relocations (car imports))))

    (get-output-bytevector port)))

(define (object-module-write object-module port)
  (let*-values
      (((elf) (make-elf elf-class/64 elf-data/2lsb))
       ((string-table) (elf-make-string-table))
       ((export-name-indices)
	(let loop ((exports (object-module-exports object-module)))
	  (if (null? exports)
	      '()
	      (cons (elf-string-table-add! string-table (export-name (car exports)))
		    (loop (cdr exports))))))
       ((got-index) (elf-string-table-add! string-table
					   (string->utf8 "_GLOBAL_OFFSET_TABLE_")))
       ((import-name-indices)
	(let loop ((imports (object-module-imports object-module)))
	  (if (null? imports)
	      '()
	      (cons (elf-string-table-add! string-table (import-name (car imports)))
		    (loop (cdr imports))))))
       ((first-import-index) (+ 5 (length (object-module-exports object-module))))
       ((text-relocations) (relocations elf (object-module-text object-module) object-module first-import-index))
       ((data-relocations) (relocations elf (object-module-data object-module) object-module first-import-index))
       ((bss-relocations) (relocations elf (object-module-bss object-module) object-module first-import-index))
       ((shstring-table) (elf-make-string-table))
       ((phoff) (elf-header-size elf))
       ((text-index) (elf-string-table-add! shstring-table (string->utf8 ".text")))
       ((rela-text-index) (elf-string-table-add! shstring-table (string->utf8 ".rela.text")))
       ((data-index) (elf-string-table-add! shstring-table (string->utf8 ".data")))
       ((rela-data-index) (elf-string-table-add! shstring-table (string->utf8 ".rela.data")))
       ((bss-index) (elf-string-table-add! shstring-table (string->utf8 ".bss")))
       ((rela-bss-index) (elf-string-table-add! shstring-table (string->utf8 ".rela.bss")))
       ((symtab-index) (elf-string-table-add! shstring-table (string->utf8 ".symtab")))
       ((strtab-index) (elf-string-table-add! shstring-table (string->utf8 ".strtab")))
       ((shstrtab-index) (elf-string-table-add! shstring-table (string->utf8 ".shstrtab")))
       ((symbol-count) (+ 5
			  (length (object-module-exports object-module))
			  (length (object-module-imports object-module))))
       ((symbol-table-size) (* symbol-count (elf-symbol-table-entry-size elf)))
       ((rela-text-count) 0)
       ((rela-text-size) (bytevector-length text-relocations))
       ((rela-data-size) (bytevector-length data-relocations))
       ((rela-bss-size) (bytevector-length bss-relocations))
       ((text-offset text-padding)
	(offset+padding phoff (section-alignment (object-module-text object-module))))
       ((rela-text-offset rela-text-padding)
	(offset+padding (+ text-offset
			   (section-size (object-module-text object-module)))
			(elf-rela-table-alignment elf)))
       ((data-offset data-padding)
	(offset+padding (+ rela-text-offset rela-text-size)
			(section-alignment (object-module-data object-module))))
       ((rela-data-offset rela-data-padding)
	(offset+padding (+ data-offset
			   (section-size (object-module-data object-module)))
			(elf-rela-table-alignment elf)))
       ((bss-offset bss-padding)
	(offset+padding (+ rela-data-offset rela-data-size)
			(section-alignment (object-module-bss object-module))))
       ((rela-bss-offset rela-bss-padding)
	(offset+padding bss-offset
			(elf-rela-table-alignment elf)))

       ((symtab-offset) (+ rela-bss-offset rela-bss-size))
       ((symtab-pad) (modulo (- symtab-offset) (elf-symbol-table-alignment elf)))
       ((symtab-offset) (+ symtab-offset symtab-pad))

       ((strtab-offset) (+ symtab-offset symbol-table-size))

       ((shstrtab-offset) (+ strtab-offset (elf-string-table-size string-table)))

       ((shoff) (+ shstrtab-offset (elf-string-table-size shstring-table)))
       ((shpad) (modulo (- shoff) (elf-section-header-alignment elf)))
       ((shoff) (+ shoff shpad)))
    (elf-write-header elf
		      osabi/system-v
		      0
		      elf-type/rel
		      machine/x86-64
		      #x0
		      phoff
		      shoff
		      #x0
		      0
		      10
		      9
		      port) 
    ;; Text section
    (write-bytevector (make-bytevector text-padding 0) port)
    (write-bytevector (section->bytevector (object-module-text object-module))
		      port)
    (write-bytevector (make-bytevector rela-text-padding 0) port)
    (write-bytevector text-relocations port)

    ;; Data section
    (write-bytevector (make-bytevector data-padding 0) port)
    (write-bytevector (section->bytevector (object-module-data object-module))
		      port)
    (write-bytevector (make-bytevector rela-data-padding 0) port)
    (write-bytevector data-relocations port)

    ;; Bss section
    (write-bytevector (make-bytevector bss-padding 0) port)
    (write-bytevector (make-bytevector rela-bss-padding 0) port)
    (write-bytevector bss-relocations port)

    ;; Symbol table
    (write-bytevector (make-bytevector symtab-pad 0) port)
    (elf-write-symbol-table-entry elf 0 0 0
				  symbol-table-bind/local
				  symbol-table-type/notype
				  symbol-table-visibility/default
				  0 port)
    (elf-write-symbol-table-entry elf 0 0 0
				  symbol-table-bind/local
				  symbol-table-type/section
				  symbol-table-visibility/default
				  1 port)
    (elf-write-symbol-table-entry elf 0 0 0
				  symbol-table-bind/local
				  symbol-table-type/section
				  symbol-table-visibility/default
				  3 port)
    (elf-write-symbol-table-entry elf 0 0 0
				  symbol-table-bind/local
				  symbol-table-type/section
				  symbol-table-visibility/default
				  5 port)
    (for-each (lambda (export index)
		(elf-write-symbol-table-entry elf
					      index
					      (export-offset export)
					      0
					      symbol-table-bind/global
					      symbol-table-type/notype
					      symbol-table-visibility/default
					      (cadr (assq (export-section export)
							  `((,(object-module-text object-module) 1)
							    (,(object-module-data object-module) 3)
							    (,(object-module-bss object-module) 5))))
					      port))
	      (object-module-exports object-module) export-name-indices)
    (elf-write-symbol-table-entry elf
				  got-index
				  0
				  0
				  symbol-table-bind/global
				  symbol-table-type/notype
				  symbol-table-visibility/default
				  0 port)
    (for-each (lambda (import index)
		(elf-write-symbol-table-entry elf
					      index
					      0
					      0
					      symbol-table-bind/global
					      symbol-table-type/notype
					      symbol-table-visibility/default
					      0 port))
	      (object-module-imports object-module) import-name-indices)

    ;; String table
    (elf-write-string-table elf string-table port)

    ;; Section header string table
    (elf-write-string-table elf shstring-table port)

    ;; Section headers
    (write-bytevector (make-bytevector shpad 0) port)
    ;; Undefined section
    (elf-write-section-header elf
			      0
			      section-type/null
			      0
			      0
			      0
			      0
			      0
			      0
			      0
			      0
			      port)
    ;; .text
    (elf-write-section-header elf
			      text-index
			      section-type/progbits
			      (+ section-flag/alloc section-flag/execinstr)
			      0
			      text-offset
			      (section-size (object-module-text object-module))
			      0     ;; link
			      0     ;; info
			      (section-alignment (object-module-text object-module))
			      0
			      port)
    ;; .rela.text
    (elf-write-section-header elf
			      rela-text-index
			      section-type/rela
			      (+ section-flag/info-link)
			      0
			      rela-text-offset
			      rela-text-size
			      7
			      1
			      (elf-rela-table-alignment elf)
			      (elf-rela-table-entry-size elf)
			      port)
    ;; .data
    (elf-write-section-header elf data-index section-type/progbits
			      (+ section-flag/write section-flag/alloc)
			      0
			      data-offset
			      (section-size (object-module-data object-module))
			      0 0
			      (section-alignment (object-module-data object-module))
			      0
			      port)
    ;; .rela.data
    (elf-write-section-header elf
			      rela-data-index
			      section-type/rela
			      (+ section-flag/info-link)
			      0
			      rela-data-offset
			      rela-data-size
			      ;; Associated symbol table
			      7
			      ;; Section to which the relocation applies
			      3
			      (elf-rela-table-alignment elf)
			      (elf-rela-table-entry-size elf)
			      port)
    ;; .bss
    (elf-write-section-header elf bss-index section-type/nobits
			      (+ section-flag/write section-flag/alloc)
			      0
			      bss-offset
			      (section-size (object-module-bss object-module))
			      0 0
			      (section-alignment (object-module-bss object-module))
			      0
			      port)
    ;; .rela.bss
    (elf-write-section-header elf
			      rela-bss-index
			      section-type/rela
			      (+ section-flag/info-link)
			      0
			      rela-bss-offset
			      rela-bss-size
			      ;; Associated symbol table
			      7
			      ;; Section to which the relocation applies
			      5
			      (elf-rela-table-alignment elf)
			      (elf-rela-table-entry-size elf)
			      port)
    ;; .symtab
    ;; link = 5 (strtab); info = last local + 1 = 1
    (elf-write-section-header elf symtab-index section-type/symtab
			      0
			      0
			      symtab-offset
			      symbol-table-size
			      8 4
			      (elf-symbol-table-alignment elf)
			      (elf-symbol-table-entry-size elf)
			      port)
    ;; .strtab
    (elf-write-section-header elf strtab-index section-type/strtab
			      0
			      0
			      strtab-offset
			      (elf-string-table-size string-table)
			      0 0 1 0
			      port)

    ;; .shstrtab
    (elf-write-section-header elf shstrtab-index section-type/strtab
			      0
			      0
			      shstrtab-offset
			      (elf-string-table-size shstring-table)
			      0 0 1 0
			      port)
    ))

